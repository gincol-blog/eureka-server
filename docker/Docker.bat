@echo off

set accion=%1

rem #####
rem se ha de especificar el nombre de docker-machine que se desee
rem ##### 
set machine=arq-root

echo INICO
echo accion=%accion%
echo docker-machine=%machine%

IF "%accion%"=="create" goto create
IF "%accion%"=="remove" goto remove
IF "%accion%"=="start" goto start
IF "%accion%"=="stop" goto stop
IF "%accion%"=="up" goto up
IF "%accion%"=="down" goto down

:create
echo CREANDO DOCKER-MACHINE %machine%
docker-machine create --driver virtualbox --virtualbox-memory 4096 %machine%
for /f %%i in ('docker-machine ip %machine%') do set IP=%%i
echo $$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
echo DOCKER-MACHINE %machine% CREADA Y ARRANCADA EN http://%IP%
echo $$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
goto fin

:remove
echo ELIMINANDO DOCKER-MACHINE %machine%
docker-machine rm -y %machine%
echo $$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
echo DOCKER-MACHINE %machine% ELIMINADA
echo $$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
goto fin

:start
echo ARRANCANDO DOCKER-MACHINE %machine%
docker-machine start %machine%
for /f %%i in ('docker-machine ip %machine%') do set IP=%%i
echo $$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
echo DOCKER-MACHINE %machine% ARRANCADA EN http://%IP%
echo $$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
goto fin

:stop
echo PARANDO DOCKER-MACHINE %machine%
docker-machine stop %machine%
echo $$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
echo DOCKER-MACHINE %machine% PARADA
echo $$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
goto fin

:up
echo DESPLEGANDO APLICACION EN DOCKER-MACHINE %machine%
@FOR /f "tokens=*" %%i IN ('docker-machine env %machine%') DO @%%i
docker-compose up -d --build
for /f %%i in ('docker-machine ip %machine%') do set IP=%%i
echo $$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
echo APLICACION ARRANCADA EN http://%IP%
echo $$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
goto fin

:down
echo PARANDO APLICACION EN DOCKER-MACHINE %machine%
@FOR /f "tokens=*" %%i IN ('docker-machine env %machine%') DO @%%i
docker-compose down
echo $$$$$$$$$$$$$$$$$$
echo APLICACION PARADA
echo $$$$$$$$$$$$$$$$$$
goto fin

:fin
echo FIN